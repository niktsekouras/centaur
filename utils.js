fibonacci = (n, delayStep = 10) => {
  let previous_first = 0;
  let previous_second = 1;
  let next = 1;
  let iterations = 0;

  for (let i = 2; i <= n; i++) {
    next = previous_first + previous_second;
    previous_first = previous_second;
    previous_second = next;
    ++iterations;
  }

  return { fibonacciNumber: next, delay: iterations * delayStep };
};

fibonacciBinet = n => {
  const sqRootOf5 = Math.sqrt(5);
  const Phi = (1 + sqRootOf5) / 2;
  const phi = (1 - sqRootOf5) / 2;

  return Math.round((Math.pow(Phi, n) - Math.pow(phi, n)) / sqRootOf5);
};

getSimpleGuid = () => {
  return (Math.random() + Math.random()).toString();
};

getRandomNumbersArray = (length, min, max) => {
  let numbers = [];
  for (let i = -1; ++i < length; ) {
    numbers.push(Math.floor(Math.random() * (max - min + 1)) + min);
  }
  return numbers;
};

const UTILS = {
  getRandomNumbersArray: getRandomNumbersArray,
  getSimpleGuid: getSimpleGuid,
  fibonacci: fibonacci,
  fibonacciBinet: fibonacciBinet
};

module.exports = UTILS;
