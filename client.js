const amqp = require('amqplib/callback_api');
const assert = require('assert');
const { getRandomNumbersArray, getSimpleGuid } = require('./utils');
const { rpcQueue, rabbitMQConfig } = require('./config');

const inputLength = 5;
let inputCounter = inputLength;
const numbersInput = getRandomNumbersArray(inputLength, 10, 40);

amqp.connect(
  rabbitMQConfig,
  function(err, conn) {
    conn.createChannel(function(err, ch) {
      ch.assertQueue('', { exclusive: true }, function(err, q) {
        const corr = getSimpleGuid();

        ch.consume(
          q.queue,
          msg => {
            if (msg.properties.correlationId == corr) {
              let payload = null;
              try {
                payload = JSON.parse(msg.content);
              } catch (err) {
                console.log('There was error in parsing the object', err);
                process.exit(1);
              }

              assert(
                payload.hasOwnProperty('id') &&
                  payload.hasOwnProperty('input') &&
                  payload.hasOwnProperty('fibonacciNumber'),
                'Invalid Response Payload.'
              );
              const { id, input, fibonacciNumber } = payload;
              console.log(' [.] Worker id: %s -- Input: %s -- fibonacci: %s', id, input, fibonacciNumber);

              if (--inputCounter === 0) {
                conn.close();
                process.exit(0);
              }
            }
          },
          { noAck: true }
        );

        //send the numbers
        numbersInput.forEach(n => {
          ch.sendToQueue(rpcQueue, Buffer.from(n.toString()), { correlationId: corr, replyTo: q.queue });
        });
      });
    });
  }
);
