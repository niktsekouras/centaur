const FibonacciWorker = require('./fibonacci-worker');

class Worker3 extends FibonacciWorker {
  constructor() {
    const id = 3;
    super(id);
  }
}

new Worker3();
