const FibonacciWorker = require('./fibonacci-worker');

class Worker1 extends FibonacciWorker {
  constructor() {
    const id = 1;
    super(id);
  }
}

new Worker1();
