const FibonacciWorker = require('./fibonacci-worker');

class Worker2 extends FibonacciWorker {
  constructor() {
    const id = 2;
    super(id);
  }
}

new Worker2();
