const amqp = require('amqplib/callback_api');
const assert = require('assert');
const { getRandomNumbersArray, fibonacci, fibonacciBinet } = require('../utils');

const { rpcQueue, rabbitMQConfig } = require('../config');

class FibonacciWorker {
  constructor(id) {
    this.id = id;
    this.onInit();
  }

  onInit() {
    this.handleCommunication();
  }

  handleCommunication() {
    amqp.connect(
      rabbitMQConfig,
      (err, conn) => {
        conn.createChannel((err, ch) => {
          ch.assertQueue(rpcQueue, { durable: false });
          ch.prefetch(1);
          ch.consume(rpcQueue, msg => this.consumeQueue(msg, ch));
        });
      }
    );
  }

  consumeQueue(msg, ch) {
    let number = parseInt(msg.content);

    const { fibonacciNumber, delay } = fibonacci(number);

    const resultObj = {
      id: this.id,
      input: number,
      fibonacciNumber: fibonacciNumber,
      delay: delay
    };

    setTimeout(function() {
      ch.sendToQueue(msg.properties.replyTo, Buffer.from(JSON.stringify(resultObj)), {
        correlationId: msg.properties.correlationId
      });

      ch.ack(msg);
    }, delay);
  }
}

module.exports = FibonacciWorker;
