# Centaur code challenge

Basic task management system using amqp protocol and RabbitMQ server.

Independent workers accept a number, calculate the Fibonacci sequence for this number and return it along with their id.

## Getting Started

git clone https://bitbucket.org/niktsekouras/centaur.git

npm i

You can set the number of tasks and the range for the random numbers in client.js file at line 8 (getRandomNumbersArray(inputLength, 10, 40))

Open a terminal and start the workers with **npm run start:workers**

Open another terminal to start the client with **npm run start:client**


### Prerequisites

You need to install the RabbitMQ server (https://www.rabbitmq.com/download.html) and ERLANG (http://www.erlang.org/downloads)

### Installation

This demo app uses the default config values for connecting to RabbitMQ server.

You can set your own RabbitMQ server options at **config.js** at **rabbitMQConfig** property.



