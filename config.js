const CONFIG = {
  rpcQueue: 'centaurTest',
  rabbitMQConfig: {
    protocol: 'amqp',
    hostname: 'localhost',
    port: 5672,
    username: 'guest',
    password: 'guest'
  }
};

module.exports = CONFIG;
